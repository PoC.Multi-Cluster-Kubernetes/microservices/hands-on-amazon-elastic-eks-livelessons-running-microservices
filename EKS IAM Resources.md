
Create Policies
  Give access to the users to do certain thenigs

Create Group
  Assign permissions to eks Group

Create Users

Create Roles
  Give access to services

Access Resources


## IAM Policies

### Create an IAM policy that allows us access to EKS and ECR 
### We will call this EKS full access. Allows access to all EKS and ECR resources
```
  "Sid" :"EKSFullAccess"
  "Effect": "Allow",
  "Action": ["eks:*",
            "ecr:*"],
  "Resource" : "*"
```

## IAM Groups
```
EKSDemoGroup

  AmazonEC2FullAccess
  AmazonS3FullAccess
  AmazonSNSReadOnlyAccess
  AmazonVPCFullAccess
  IAMReadOnlyAccess
  EKSFullAccess
  AWSCloudFormationFullAccess
  IAMPAssThrough
```

## IAMRules
In our case is a user cloud_user

## IAMRoles
  AWSServiceRoleForAmazonEKS - Allows EKS to manage clusters on your behalf
  EKSClusterRole - Allows Access to other AWS service resources that are not rewuired yto operate clusters managed by EKS.

## Creating Resources with Terraform
```
TRACE, DEBUG, INFO, WARN or ERROR 
export TF_LOG=INFO

terraform output
terraform output -raw password | base64 --decode | gpg --decrypt
terraform output -raw secret | base64 --decode | gpg --decrypt

```



 



