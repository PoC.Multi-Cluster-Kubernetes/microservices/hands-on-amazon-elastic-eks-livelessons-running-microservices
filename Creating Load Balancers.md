

kubectl get svc

kubectl edit svc ngx
    Change clusterIP to LoadBalancer

To change to an NLB we can simply add the annotation to our service:

```
    metadata:
        annotations:
            ...
            service.beta.kubernetes.io/aws-load-balancer-type: nlb
        ...
    ...
```


