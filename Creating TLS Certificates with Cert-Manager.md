


Cert-manager 

- Open-source kuberntes native certificate manager


 ![Cert-manager Architecture](/images/cert-manage.png)
 


Link: https://github.com/vallard/K8sClass/blob/master/04/TLS.md
\
code: https://github.com/vallard/K8sClass/tree/master/04/cert-manager


```
kubectl apply -f https://github.com/jetstack/cert-manager/releases/download/v1.10.0/cert-manager.yaml

kubectl get pods -n cert-manager

cd cert-manager
kubectl create -f staging-issuer.yaml

kubectl create -f prod-issuer.yaml

kubectl get clusterissuers

    NAME                  READY   AGE
    letsencrypt-prod      True    96s
    letsencrypt-staging   True    108s


ngx-ing.yaml
    apiVersion: networking.k8s.io/v1
    kind: Ingress
    metadata:
    annotations:
        cert-manager.io/cluster-issuer: letsencrypt-prod
        kubernetes.io/ingress.class: nginx
    name: ngx
    namespace: default
    spec:
    tls:
    - hosts:
        - k8s.castlerock.ai
        secretName: ngx-tls-cert
    rules:
        - host: k8s.castlerock.ai
        http:
            paths:
            - path: "/"
                pathType: Prefix
                backend:
                service:
                    name: ngx
                    port:
                    number: 80

kubectl get orders
kubectl get certificates
kubectl get certificaterequests

```
