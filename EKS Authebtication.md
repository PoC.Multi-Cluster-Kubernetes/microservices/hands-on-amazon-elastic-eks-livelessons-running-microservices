

## EKS Athentication


List clusters in AWS and some extra commands


```
    aws eks list-clusters
    kubectl config get-contexts
    aws eks update-kubeconfig --name <cluster-name>
    kubectl config get-contexts
    kubectl cluster-info
    kubectl use-context <cluster-name>
```
