 

kubectl get svc

kubectl edit svc ngx
    Change LoadBalancer to clusterIP (if the nginx is defined as LoadBalancer)


Deploy Nginx Ingress Controller
```
kubectl apply -f https://raw.githubusercontent.com/kubernetes/ingress-nginx/controller-v0.41.2/deploy/static/provider/aws/deploy.yaml

```

kubectl get svc --all-namespaces

