

Namespaces

kubectl get nodes -o wide
kubectl get ns
kubectl get all -n kube-system


Pods
kubectl describe pods -n <namespace> <pod-name>

DaemonSets - 1 pod in each of the nodes

kubectl get ds -n <namespace>
kubectl describe ds -n <namespace> <ds-name>


ReplicaSet
kubectl get deploy -n <namespace> <deployment-name>


Examining Networking

Worker node 
    Kubelet 
    kube-proxy

Manster
    kube-controller manager
    etcd
    kube scheduler
    cloud-controller manager
    kube-api-server
       --> kubelet
       --> kube-proxy